n = input()
roads = {}

for i in xrange(n-1):
	u, v = map(int, raw_input().split(' '))
	roads[i+1] = (u, v, )

days = []
while roads:
	busy = {}
	days.append({
		'd': 0,
		'roads': [],
	})
	for ind, (u, v) in roads.items():
		if u not in busy and v not in busy:
			busy[u] = 1
			busy[v] = 1
			days[-1]['d'] += 1
			days[-1]['roads'].append(ind)
			del roads[ind]


print len(days)
for day in days:
	print day['d'], ' '.join(map(str, day['roads']))
