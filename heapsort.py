import math
import random
import sys


def i_parent(i):
	return int(math.floor((i-1) / 2))


def i_left_child(i):
	return 2*i + 1


def i_right_child(i):
	return 2*i + 2


def heapsort(a):
	a = heapify(a)
	end = len(a) - 1
	while end >= 0:
		a[end], a[0] = a[0], a[end]
		end -= 1
		sift_down(a, 0, end)
	return a


def heapify(a):
	start = i_parent(len(a) - 1)
	while start >= 0:
		sift_down(a, start, len(a) - 1)
		start -= 1
	return a



def sift_up(a):
	ind = len(a) - 1
	parent = i_parent(ind)
	while ind > 0 and a[ind] > a[parent]:
		a[ind], a[parent] = a[parent], a[ind]
		ind = parent
		parent = i_parent(ind)


def sift_down(a, start, end):
	ind = start
	while i_left_child(ind) <= end:
		l = i_left_child(ind)
		swap = ind
		if a[swap] < a[l]:
			swap = l
		if l+1 <= end and a[swap] < a[l+1]:
			swap = l+1
		if swap == ind:
			break
		a[ind], a[swap] = a[swap], a[ind]
		ind = swap

if __name__ == '__main__':
	a = [random.randint(0, 100) for i in xrange(int(sys.argv[1]))]
	a = heapsort(a)
	print a == sorted(a)
