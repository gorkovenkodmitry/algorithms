import random
import sys



def radixsort(a):
	if not a:
		return a
	def d(n, j):
		return (n / (10**j)) % 10

	w = len(unicode(max(a)))
	for i in xrange(w):
		l = []
		for j in xrange(10):
			l.append([])
		for x in a:
			l[d(x, i)].append(x)
		a = []
		for j in xrange(10):
			a += l[j]
	return a


if __name__ == '__main__':
	a = [random.randint(0, 100000) for i in xrange(int(sys.argv[1]))]

	a = radixsort(a)
	print a == sorted(a)
