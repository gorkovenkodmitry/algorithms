import random
import sys


def quicksortr(a, l, r):
	if l < r:
		p = partition(a, l, r)
		quicksortr(a, l, p-1)
		quicksortr(a, p+1, r)
	return a


def quicksort(a):
	stack = [(0, len(a)-1)]
	while stack:
		l, r = stack[0]
		del stack[0]
		while l < r:
			p = partition(a, l, r)
			stack.append((p+1, r))
			r = p - 1
	return a


def median(a, b, c):
	if b > a:
		a, b = (b, a)
	if c > b:
		b, c = (c, b)
	if b > a:
		a, b = (b, a)
	return b


def partition(a, l, r):
	p = median(a[l], a[r], a[(r+l)/2])
	pind = l
	if p == a[r]:
		pind = r
	if p == a[(r+l)/2]:
		pind = (r+l)/2
	j = l
	while l <= j <= r:
		if a[j] < p and j > pind:
			swap = a[j]

			a[pind:j] = a[pind:j]
			a[pind] = swap
			# a.insert(pind, a[j])
			# del a[j+1]
			pind += 1
		elif a[j] > p and j < pind:
			a.insert(pind+1, a[j])
			del a[j]
			j -= 1
			pind -= 1
		j += 1
	return pind


# def partition(a, l, r):
# 	p = a[r]
# 	i = l
# 	for j in xrange(l, r):
# 		if a[j] < p:
# 			a[i], a[j] = a[j], a[i]
# 			i += 1
# 	a[i], a[r] = a[r], a[i]
# 	return i

if __name__ == '__main__':
	a = [random.randint(0, 100) for i in xrange(int(sys.argv[1]))]
	a = quicksort(a)
	print a == sorted(a)
