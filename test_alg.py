# coding: utf-8
import datetime
import random
import sys
import quicksort
import heapsort
import radixsort

def time_meter(alg, a):
	t1 = datetime.datetime.now()
	a = alg(a)
	print alg.func_name
	return u'%s' % (datetime.datetime.now() - t1)


n = sys.argv[1]
a = [random.randint(0, 1000000) for i in xrange(int(sys.argv[1]))]
# a = sorted(a)
print time_meter(heapsort.heapsort, a[:])
print time_meter(radixsort.radixsort, a[:])
print time_meter(quicksort.quicksort, a[:])
